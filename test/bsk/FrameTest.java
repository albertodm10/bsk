package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.*;


public class FrameTest {

	@Test
	public void testCalculateFrameFirstThrow() throws BowlingException {
		Frame frame = new Frame(2, 4);
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void testCalculateFrameSecondThrow() throws BowlingException {
		Frame frame = new Frame(2, 4);
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test(expected=BowlingException.class)
	public void testWrongInputFrame() throws BowlingException {
		Frame frame = new Frame(5, 11);
		frame.getSecondThrow();
	}
	
	@Test
	public void testCalculateFrameScore() throws BowlingException {
		Frame frame = new Frame(2, 6);
		assertEquals(8, frame.getScore());
	}
	
	@Test
	public void testVerifySpareFrame() throws BowlingException {
		Frame frame = new Frame(1, 9);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testVerifyStrikeFrame() throws BowlingException {
		Frame frame = new Frame(10, 0);
		assertTrue(frame.isStrike());
	}
	
	@Test(expected=BowlingException.class)
	public void testWrongInputFrameAfterStrikeAtFirstThrow() throws BowlingException {
		Frame frame = new Frame(10, 8);
		frame.getSecondThrow();
	}
	
}
