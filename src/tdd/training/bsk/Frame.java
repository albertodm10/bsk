package tdd.training.bsk;

public class Frame {
	
	public static final int PINS_NUMBER = 10;
	
	private int firstThrowScore;
	private int secondThrowScore;
	private int bonusScore;

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		if(firstThrow < 0 || firstThrow > PINS_NUMBER || secondThrow < 0 || secondThrow > PINS_NUMBER) {
			throw new BowlingException("Incorrect framescore");
		}
		if(firstThrow == PINS_NUMBER && secondThrow != 0) {
			throw new BowlingException("Incorrect framescore");
		}
		firstThrowScore = firstThrow;
		secondThrowScore = secondThrow;
		bonusScore = 0;
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		
		return firstThrowScore;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		
		return secondThrowScore;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		bonusScore = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {

		return bonusScore;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		
		return firstThrowScore + secondThrowScore + bonusScore;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		return firstThrowScore == PINS_NUMBER;
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		return firstThrowScore < PINS_NUMBER && firstThrowScore + secondThrowScore == PINS_NUMBER;
	}

}
