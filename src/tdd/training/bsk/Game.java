package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	public static final int FRAMES_NUMBER = 10;
	
	private ArrayList <Frame> gameFrames;
	private int firstBonusThrowScore;
	private int secondBonusThrowScore;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.gameFrames = new ArrayList<>();
		firstBonusThrowScore = 0;
		secondBonusThrowScore = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(gameFrames.size() >= FRAMES_NUMBER) {
			throw new BowlingException("Exceeding number of Frames");
		}

		gameFrames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to FRAMES_NUMBER-1) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index<0 || index > FRAMES_NUMBER-1) {
			throw new BowlingException("Incorrect index of Frame");
		}
		
		return gameFrames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if(!gameFrames.get(FRAMES_NUMBER-1).isSpare() && !gameFrames.get(FRAMES_NUMBER-1).isStrike()) {
			throw new BowlingException("You are unable to set a bonus throw");
		}
		firstBonusThrowScore = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if(!gameFrames.get(FRAMES_NUMBER-1).isStrike()) {
			throw new BowlingException("You are unable to set a bonus throw");
		}
		secondBonusThrowScore = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		
		return firstBonusThrowScore;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		
		return secondBonusThrowScore;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		if(gameFrames.isEmpty()) {
			throw new BowlingException("No values found. Impossible to calculate score.");
		}
		int score = 0;
		
		for(int i=0; i<FRAMES_NUMBER; i++) {
			if(gameFrames.get(i).isStrike()) {
				gameFrames.get(i).setBonus(calculateStrikeBonusScore(i));
			}
			else if(gameFrames.get(i).isSpare()) {
				gameFrames.get(i).setBonus(calculateSpareBonusScore(i));
			}
			
			score = score + gameFrames.get(i).getScore();
		}
		
		return score + firstBonusThrowScore + secondBonusThrowScore;	
	}
	
	/**
	 * It returns the bonus score of the strike.
	 * 
	 * @param index The index of the referred frame with strike.
	 * @return The bonus score of the strike. 
	 */
	private int calculateStrikeBonusScore(int index) {
		if(index < FRAMES_NUMBER-1) {
			if(gameFrames.get(index+1).isStrike()) {
				if(index<FRAMES_NUMBER-2) {
					return Frame.PINS_NUMBER + gameFrames.get(index+2).getFirstThrow();
				}
				else {
					return Frame.PINS_NUMBER + firstBonusThrowScore;
				}
			}
			else {
				return gameFrames.get(index+1).getFirstThrow() + gameFrames.get(index+1).getSecondThrow();
			}
		}
		return 0;
	}
	
	/**
	 * It returns the bonus score of the spare.
	 * 
	 * @param index The index of the referred frame with spare.
	 * @return The bonus score of the spare. 
	 */
	private int calculateSpareBonusScore(int index) {
		if(index < FRAMES_NUMBER-1) {
			return gameFrames.get(index+1).getFirstThrow();
		}
		return 0;
	}

}
